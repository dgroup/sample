package sample;

import junit.framework.TestCase;

public class SampleTest extends TestCase {

	public void testChangeNum(){
		String beforeStr="100";
		String afterStrNum="100";
		String afterStrNum2="10";

		Sample ts = new Sample();
		String resultInt=ts.changeNum(beforeStr);

		assertEquals("数値に変換されていない",afterStrNum,resultInt);
		assertEquals("数値に変換されていない222",afterStrNum2,resultInt);
	}
}

