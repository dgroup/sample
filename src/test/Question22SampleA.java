package test;


/* 親クラスであるQuestion22SampleBに引数無しコンストラクタが無い為、
 * コンパイラが自動生成するデフォルトコンストラクタ Question22SampleA()
 * で呼び出せていない為エラーになる
 *
 * (デフォルトコンストラクタは、スーパークラスの引数なしコンストラクタを呼ぶ為）
 */
public class Question22SampleA extends Question22SampleB {
	void speak(){
		System.out.println(name);
	}

	public static void main(String[] args){
		Question22SampleA s = new Question22SampleA();
		s.speak();
	}
}
