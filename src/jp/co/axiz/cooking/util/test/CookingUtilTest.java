package jp.co.axiz.cooking.util.test;

import java.util.ArrayList;

import jp.co.axiz.cooking.util.CookingUtil;
import junit.framework.TestCase;

/**
 *
 * @author t.sugai
 *
 */
public class CookingUtilTest extends TestCase {
	public void testGetIngredients() {
		//『何をつくりますか？1:カレー　2:チャーハン』で使用するメソッドのテストです。
		//今回はカレーを選択したとします。
		int selectNumber = 1;

		//上記で初期化したselectNumberを引数としてgetIngredientsメソッドを呼び出します。
		//今回のテストではgetIngredientsメソッドの戻り値としてgetCarryメソッド内の配列内情報を順番通りに全て取得します。
		ArrayList<String> list = CookingUtil.getIngredients(selectNumber);


		//処理結果と想定された処理結果を比較、確認します。
		//assertEqualsメソッドの第２引数と第３引数の値が一致すれば成功です。
		assertEquals("選択どおりにカレーの食材を取ってこれていない", "ルー", list.get(0));			//getCarry()[0]のデータ
		assertEquals("選択どおりにカレーの食材を取ってこれていない",  "タマネギ", list.get(1));		//getCarry()[1]のデータ
		assertEquals("選択どおりにカレーの食材を取ってこれていない", "ジャガイモ", list.get(2));	//getCarry()[2]のデータ
		assertEquals("選択どおりにカレーの食材を取ってこれていない", "ニンジン", list.get(3));		//getCarry()[3]のデータ
		assertEquals("選択どおりにカレーの食材を取ってこれていない", "肉", list.get(4));			//getCarry()[4]のデータ
	}

}
