
//第26問
package a26;

class SampleA extends b26.SampleB {
	String name;
	public SampleA(String name){
		super(name);
	}
	public void print() {
		System.out.println(this.name + "さんです。");
	}
	public static void main(String[] args){
		b26.SampleB sb = new SampleA("山田");
		SampleA sa = (SampleA)sb;
		sa.print();
	}
}