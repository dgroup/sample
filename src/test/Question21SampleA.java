package test;

public class Question21SampleA extends Question21SampleB {
	@Override
	public String print(){
		return name + "さんは" + address + "に住んでます。";
	}

	public static void main(String[] args){

		Question21SampleB b = new Question21SampleA();
		b.name = "山田";
		b.old = 20;
		b.address = "東京";

		System.out.println(b.print());

	}

}
