package test;
public class Question28SampleA {
	Question28SampleA sa;
	public Question28SampleA() {
	}
	public Question28SampleA(Question28SampleA sa){
		this.sa = sa;
	}
	public static void main(String[] args) {
		Question28SampleA sa1 = new Question28SampleA(new Question28SampleA());
		Question28SampleA sa2 = new Question28SampleA();
		Question28SampleA sa3 = new Question28SampleA();
		sa2 = sa1;
		sa2 = sa3;
		sa3 = null;
		sa1 = sa3;
	}
}