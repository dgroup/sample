package hukusyu0519;

public class PlacedRectangle5 extends Rectangle {
	int x, y;

	PlacedRectangle5(){
		this(0, 0);
	}

	PlacedRectangle5(int x, int y){
		this.x = x;
		this.y = y;
	}

	PlacedRectangle5(int x, int y, int width, int height){
		super(width,height);
		this.x = x;
		this.y = y;
	}

	void setLocation(int x, int y){
		this.x = x;
		this.y = y;
	}

	@Override
	public String toString(){
		return "[ (" + x + ", " + y + ") [" + width + ", " + height + "]]";
	}
}
