package hukusyu0519;

import java.io.IOException;

public class ExeptionTest5_5 {
	public static void main(String[] args){
		try{
			method1(0);
			method2(0);
			method3(0);

		}catch (IOException e){
			System.out.println("method1");
		}catch (NumberFormatException e){
			System.out.println("method2");
		}catch (Exception e){
			System.out.println("method3");
		}

	}
	static void method1(int x) throws Exception{
		throw new IOException();
	}
	static void method2(int x) throws Exception{
		throw new NumberFormatException();
	}
	static void method3(int x) throws Exception{
		throw new Exception();
	}
}
