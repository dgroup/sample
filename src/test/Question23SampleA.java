package test;

/*
 * 抽象クラスを継承しているが、抽象メソッドspeakを実装していない
 */
public class Question23SampleA extends Question23SampleB {
	public static void main(String[] args) {
		Question23SampleB s = new Question23SampleA();
		s.speak();
	}
	/*
	@Override
	void speak(){
		System.out.println("test");
	}
	 */
}
