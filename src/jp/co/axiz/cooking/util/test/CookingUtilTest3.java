package jp.co.axiz.cooking.util.test;

import java.util.ArrayList;
import jp.co.axiz.cooking.util.CookingUtil;
import junit.framework.TestCase;

/**
 *
 * @author t.sugai
 *
 */
public class CookingUtilTest3 extends TestCase {
	public void testSetIngredients() {
		//『食材を足す』選択時に使用するメソッドのテストです。
		//※ここでは一つだけ記述した際の動作を確認します。
		ArrayList<String> list = new ArrayList<String>();

		//今回追加する食材名はシーフードとします。
		String ingredient = "シーフード";

		//上記で作成したlistと追加したい食材を引数としてsetIngredientsを呼び出し、メソッド 内処理でlistに追加します。
		//今回のテストにおけるsetIngredientsメソッドの戻り値はありません。
		//今回のテストにおいては、ingredient変数値が0番目の部屋に代入された状態のlistインスタンスが作成されます。
		CookingUtil.setIngredients(list,ingredient);

		
		//処理結果と想定された処理結果を比較、確認します。
		//ingredient変数のデータがlistの0番目の部屋に追加できていれば成功です。
		assertEquals("追加されていない", ingredient, list.get(0));
	}
}
