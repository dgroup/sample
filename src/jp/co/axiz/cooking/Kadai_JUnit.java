package jp.co.axiz.cooking;


//add wrote by tagashira


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

import jp.co.axiz.cooking.util.CookingUtil;


//teatagaweiofhoajelaf
//teatagaweiofhoajelaf
/**
 *
 * @author t.sugai
 *
 */
public class Kadai_JUnit {
	public static void main(String args[]) {
		ArrayList<String> list = null;			//食材格納用変数
		try {
			System.out.println("何を作りますか?");
			System.out.println("1:カレー　2:チャーハン");

			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			String line = reader.readLine();
			int selectNumber = Integer.parseInt(line);

			// 選んだ料理別に食材を取得してlistに入れる
			list = CookingUtil.getIngredients(selectNumber);

			if (list == null) {
				throw new NullPointerException();
			}
			System.out.println("食材を取り出しました");
			while (true) {
				for (int i = 0; i < list.size(); i++) {
					System.out.println(i + 1 + ":" + list.get(i));
				}
				System.out.println("どうしますか？");
				System.out.println("1:これで作る　2:食材を足す　3:食材を減らす");
				line = reader.readLine();
				int selectTask = Integer.parseInt(line);
				switch (selectTask) {
				case 1:
					if (list.size() == 0) {
						System.out.println("食材がありません");
					} else {
						System.out.println("ジャージャー");
						System.out.println("いただきます");
						System.out.println("モグモグ");
						System.out.println("ごちそうさまでした");
						break;
					}
				case 2:
					System.out.println("足したい食材を書いてください");
					System.out.println("※複数の場合はカンマで区切り、スペースを入れずに入力してください");
					line = reader.readLine();

					//入力された食材を足す
					CookingUtil.setIngredients(list,line);

					selectTask = 2;
					break;
				case 3:
					System.out.println("減らしたい食材の番号を入力してください");
					line = reader.readLine();
					int selectReduce = Integer.parseInt(line);

					ArrayList<String> oldList = new ArrayList<String>();

					for (int i = 0; i < list.size(); i++) {
						oldList.add(list.get(i));
					}

					//入力された食材を減らす
					list = CookingUtil.reduceIngredients(list, selectReduce);

					if (list.size() == 0) {
						System.out.println("現在食材はありません");
					} else if (selectReduce >= list.size()) {
						System.out.println("現在食材番号は" + list.size() + "までです");
					} else if (selectReduce <= 0) {
						System.out.println("入力された値が不正です");
					} else {
						System.out.println("選択された食材を冷蔵庫に戻しました");
					}
					break;
				default:
					System.out.println("1〜3で選んでください");
					break;
				}
				if (selectTask == 1) {
					break;
				}
			}
		} catch (NumberFormatException e) {
			System.out.println("整数を入力してください");
		} catch (NullPointerException e) {
			System.out.println("1か2で選んでください");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}