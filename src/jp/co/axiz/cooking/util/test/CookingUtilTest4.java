package jp.co.axiz.cooking.util.test;

import java.util.ArrayList;
import jp.co.axiz.cooking.util.CookingUtil;
import junit.framework.TestCase;

/**
 *
 * @author t.sugai
 *
 */
public class CookingUtilTest4 extends TestCase {
	public void testSetIngredients() {
		//『食材を足す』選択時に使用するメソッドのテストです。
		//※ここでは複数記述した際の動作を確認します。
		ArrayList<String> list = new ArrayList<String>();

		//今回追加する食材名はシーフードとします。
		String ingredients = "シーフード,シナモン,コリアンダー";

		//上記で作成したlistと追加したい食材を引数としてsetIngredientsを呼び出し、メソッド 内処理でlistに追加します。
		//今回のテストにおけるsetIngredientsメソッドの戻り値はありません。
		//今回のテストにおいては、ingredient変数値においてカンマで分けられている食材が0番目の部屋から順番に代入された状態のlistインスタンスが返ります。
		CookingUtil.setIngredients(list,ingredients);

		
		// 処理結果と想定された処理結果を比較、確認します。
		//ingredient変数の1カンマ目までのデータ(カンマは含まない)がlistの0番目の部屋に追加できれば成功です。
		//ingredient変数の1カンマ目から2カンマ目までのデータ(カンマは含まない)がlistの1番目の部屋に追加できれば成功です。
		//ingredient変数の2カンマ目以降のデータ(カンマは含まない)がlistの2番目の部屋に追加できれば成功です。
		assertEquals("個別に追加されていない", "シーフード", list.get(0));
		assertEquals("2つめ以降が個別に追加されていない", "シナモン", list.get(1));
		assertEquals("3つめ以降が個別に追加されていない", "コリアンダー", list.get(2));
	}
}
