package jp.co.axiz.cooking.util;

import java.util.ArrayList;


/**
 *
 * @author t.sugai
 *
 */
	public class CookingUtil {


	//カレーの材料
	//このメソッドを呼び出すことでカレーの食材をString配列で取得できる
	private static String[] getCarry() {
		String[] ingredients = { "ルー", "タマネギ", "ジャガイモ", "ニンジン", "肉" };
		return ingredients;
	}

	//チャーハンの材料
	//このメソッドを呼び出すことでチャーハンの食材をString配列で取得できる
	private static String[] getCharhang() {
		String[] ingredients = { "ごはん", "卵", "しょうゆ", "ごま油", "ハム" };
		return ingredients;
	}

	// 選んだ料理別に食材を取得してlistに入れる
	public static ArrayList<String> getIngredients(int i){

		//戻り値用のArrayListを生成
		ArrayList<String> list = new ArrayList<String>();

		//食材を格納するStringの配列を宣言
		String[] a;


		if(i==1){

			//1が選択された時カレーの食材を格納
			 a=getCarry();
			 for(int j = 0; j<a.length;j++){
					list.add(a[j]);
			 }
			 return list;

		}else if(i==2){

			//2が選択された時チャーハンの食材を格納
			 a=getCharhang();
			 for(int j = 0; j<a.length;j++){
					list.add(a[j]);
			 }

			 //格納したlistを返す
			 return list;

		}else{
			//1.2以外の場合nullを返す
			return null;
		}
	}

	//入力された食材を足す
	public static void setIngredients(ArrayList<String> list,String str){

		//入力された食材が複数だった場合、食材ごとに配列に代入
		String[] strAry = str.split(",");

		//入力された食材の数だけArrayListに追加
		for(int i = 0;i<strAry.length;i++){
		list.add(strAry[i]);
		}
	}

	//入力された食材を減らす
	public static ArrayList<String> reduceIngredients(ArrayList<String> old,int num){

		//戻り値用のArrayListを生成
		ArrayList<String> list = new ArrayList<String>();

		//食材の数だけ繰り返し判定
		for(int i=0;i<old.size();i++){

			//食材番号が削除する食材番号と同じとき戻り値のListに代入しない
			if(i==num-1){

			}else{

			//食材番号が削除する食材番号以外の場合はそのまま戻り値のListに代入
			list.add(old.get(i));
			}
		}

		//食材を削除したArrayListを返す
		return list;
	}

}