package jp.co.axiz.cooking.util.test;

import java.util.ArrayList;

import jp.co.axiz.cooking.util.CookingUtil;
import junit.framework.TestCase;

/**
 *
 * @author t.sugai
 *
 */
public class CookingUtilTest6 extends TestCase {
	public void testSetIngredients() {
		//『食材を減らす』選択時に使用するメソッドのテストです。
		//※食材番号…料理を選択した際に表示される食材リストの番号

		//list 食材データ取得用です。
		//oldList list内に存在する食材をコピーするための変数（検証用）です。
		ArrayList<String> list = new ArrayList<String>();
		ArrayList<String> oldList = new ArrayList<String>();

		//今回はカレーを選択したとしてselcetNumberを1で初期化します。
		int selectNumber = 1;
		//削除したい食材番号。今回は取得したlist内に存在しない食材番号6をセットします。
		int selectReduce = 6;

		//材料名をArrayList型の戻り値で返すgetIngredientsを呼び出し、結果をlistに代入します。
		//今回のテストではカレーの材料です。
		list = CookingUtil.getIngredients(selectNumber);

		//reduceIngredientsメソッド通過前にlist内に存在する食材をoldListに退避させます。
		for(int i = 0; i < list.size(); i++){
			oldList.add(list.get(i));
		}

		//listとselectReduceを引数としてreduceIngredientsを呼び出し、戻り値を再度listに代入します。
		//今回のテストにおけるreduceIngredientsメソッドの戻り値はlistインスタンスです。
		//listの中身は全てのデータが削除された状態のカレーの材料です。（list内部のデータはありません）
		//ただし削除された食材番号の部屋にnullが入っていてはいけません。（list内部のデータはありませんが、インスタンスは存在します。）
		list = CookingUtil.reduceIngredients(list, selectReduce);


		//処理結果と想定された処理結果を比較、確認します。
		//reduceIngredients(削除用)メソッドを通過した結果のlistと通過していないoldListの値を比較します。
		//データが削除されないので、比較結果が同じであれば成功です。
		assertEquals("ルーが削除されている",oldList.get(0) , list.get(0));
		assertEquals("タマネギが削除されている",oldList.get(1) , list.get(1));
		assertEquals("ジャガイモが削除されている",oldList.get(2) , list.get(2));
		assertEquals("ニンジンが削除されている",oldList.get(3) , list.get(3));
		assertEquals("肉が削除されている",oldList.get(4) , list.get(4));
	}
}

