//��27��
package test;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Question27SampleA {
	public static void main(String[] args){
		try{
			int x = 10;
			if (x < 5){
				throw new FileNotFoundException();
			}else if (x < 10) {
				throw new IOException();
			}
			System.out.println("A");
		} catch (FileNotFoundException e) {
			System.out.println("B");
		} catch (NotFoundException e) {
			System.out.println("C");
		} catch (Exception e) {
			System.out.println("D");
		} catch (IOException e) {
			System.out.println("E");
		}
	}
}