package test;

public class Question7 {
	public static void main(String[] args){
		int year;
		for(year = 1500;year <= 1600;year++){
			if(year % 400 == 0 || year % 4 == 0 && year % 100 != 0){
				System.out.println(year + "年は閏年です。");
			}
		}
	}
}
